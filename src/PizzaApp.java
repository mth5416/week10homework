/**
 * PizzaApp.java
 * Application for ordering pizza
 *
 * @author Mason Hicks
 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.Iterator;

public class PizzaApp extends Application {

    //Instance Variables
    private ArrayList<Pizza> pizzas;
    private Pizza currentPizza;
    private RadioButton small, medium, large;
    private CheckBox pepper, onion, mushroom, olive, bananaPepper;
    private CheckBox ham, pepperoni, sausage, salami, chicken, steak;

    private HBox size;
    private VBox veggieToppings1, veggieToppings2, meatToppings1, meatToppings2;
    private HBox veggieToppings, meatToppings;
    private Label veggieLabel, meatLabel;
    private VBox veggies, meats;

    private HBox toppings;

    private VBox menu;
    private VBox everything;

    private HBox tooManyBoxes;

    private Label currentPizzaLabel;
    private Button addCurrentPizza;
    private Button finishOrder;
    private HBox bottom;

    private Scene finishedOrder;
    private Label finishedOrderText;
    private Stage stage;

    /**
     * Creates the application window
     * @param primaryStage Stage
     * @throws Exception idk
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene;
        double spacing = 5;
        pizzas = new ArrayList<>();
        stage = primaryStage;
        ToggleGroup sizeGroup = new ToggleGroup();
        currentPizza = new Pizza();

        stage.setTitle("Pete's Pizza");

        small = new RadioButton("Small: $10");
        medium = new RadioButton("Medium: $11");
        large = new RadioButton("Large: $12");

        small.setOnAction(new ToppingListener());
        medium.setOnAction(new ToppingListener());
        large.setOnAction(new ToppingListener());
        small.setSelected(true);

        small.setToggleGroup(sizeGroup);
        medium.setToggleGroup(sizeGroup);
        large.setToggleGroup(sizeGroup);

        pepper = new CheckBox("Peppers");
        onion = new CheckBox("Onions");
        mushroom = new CheckBox("Mushrooms");
        olive = new CheckBox("Olives");
        bananaPepper = new CheckBox("Bananna Peppers");

        pepper.setOnAction(new ToppingListener());
        onion.setOnAction(new ToppingListener());
        mushroom.setOnAction(new ToppingListener());
        olive.setOnAction(new ToppingListener());
        bananaPepper.setOnAction(new ToppingListener());

        ham = new CheckBox("Ham");
        pepperoni = new CheckBox("Pepperoni");
        sausage = new CheckBox("Sausage");
        salami = new CheckBox("Salami");
        chicken = new CheckBox("Chicken");
        steak = new CheckBox("Steak");

        ham.setOnAction(new ToppingListener());
        pepperoni.setOnAction(new ToppingListener());
        sausage.setOnAction(new ToppingListener());
        salami.setOnAction(new ToppingListener());
        chicken.setOnAction(new ToppingListener());
        steak.setOnAction(new ToppingListener());

        veggieLabel = new Label("Veggies\n$0.50/ea");
        meatLabel = new Label("Meats\n$2 for one, $1.50/ea for >1");

        size = new HBox(small, medium, large);
        size.setSpacing(spacing);
        size.setAlignment(Pos.CENTER);

        veggieToppings1 = new VBox(pepper, onion, mushroom);
        veggieToppings1.setSpacing(spacing);
        veggieToppings2 = new VBox(olive, bananaPepper);
        veggieToppings2.setSpacing(spacing);
        veggieToppings = new HBox(veggieToppings1, veggieToppings2);
        veggieToppings.setSpacing(spacing);
        veggies = new VBox(veggieLabel, veggieToppings);

        meatToppings1 = new VBox(ham, pepperoni, sausage);
        meatToppings1.setSpacing(spacing);
        meatToppings2 = new VBox(salami, chicken, steak);
        meatToppings2.setSpacing(spacing);
        meatToppings = new HBox(meatToppings1, meatToppings2);
        meatToppings.setSpacing(spacing);
        meats = new VBox(meatLabel, meatToppings);

        toppings = new HBox(veggies, meats);
        toppings.setSpacing(spacing);

        menu = new VBox(size, toppings);
        menu.setSpacing(spacing);
        menu.setPadding(new Insets(10));

        currentPizzaLabel = new Label(currentPizza.toString());
        currentPizzaLabel.setMinSize(200,225);
        addCurrentPizza = new Button("Add to order");
        finishOrder = new Button("Complete Order");

        addCurrentPizza.setOnAction(new AddToOrder());
        finishOrder.setOnAction(new CompleteOrder());

        tooManyBoxes = new HBox(menu, currentPizzaLabel);
        bottom = new HBox(addCurrentPizza, finishOrder);
        bottom.setAlignment(Pos.CENTER);
        bottom.setSpacing(20);

        everything = new VBox(tooManyBoxes, bottom);

        scene = new Scene(everything);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Main
     * @param args String[] args
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Class to update pizza when toppings are selected and update sidebar with current order
     */
    class ToppingListener implements EventHandler<ActionEvent>{

        /**
         * Updates pizza depending on selections
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(pepper.isSelected()){
                currentPizza.addVeggie(Pizza.Veggies.PEPPER);
            } else {
                currentPizza.removeVeggie(Pizza.Veggies.PEPPER);
            }
            if(onion.isSelected()){
                currentPizza.addVeggie(Pizza.Veggies.ONION);
            } else {
                currentPizza.removeVeggie(Pizza.Veggies.ONION);
            }
            if(mushroom.isSelected()){
                currentPizza.addVeggie(Pizza.Veggies.MUSHROOM);
            } else {
                currentPizza.removeVeggie(Pizza.Veggies.MUSHROOM);
            }
            if(olive.isSelected()){
                currentPizza.addVeggie(Pizza.Veggies.OLIVE);
            } else {
                currentPizza.removeVeggie(Pizza.Veggies.OLIVE);
            }
            if(bananaPepper.isSelected()){
                currentPizza.addVeggie(Pizza.Veggies.BANANA_PEPPER);
            } else {
                currentPizza.removeVeggie(Pizza.Veggies.BANANA_PEPPER);
            }

            if(ham.isSelected()){
                currentPizza.addMeat(Pizza.Meats.HAM);
            } else {
                currentPizza.removeMeat(Pizza.Meats.HAM);
            }
            if(pepperoni.isSelected()){
                currentPizza.addMeat(Pizza.Meats.PEPPERONI);
            } else {
                currentPizza.removeMeat(Pizza.Meats.PEPPERONI);
            }
            if(sausage.isSelected()){
                currentPizza.addMeat(Pizza.Meats.SAUSAGE);
            } else {
                currentPizza.removeMeat(Pizza.Meats.SAUSAGE);
            }
            if(salami.isSelected()){
                currentPizza.addMeat(Pizza.Meats.SALAMI);
            } else {
                currentPizza.removeMeat(Pizza.Meats.SALAMI);
            }
            if(chicken.isSelected()){
                currentPizza.addMeat(Pizza.Meats.CHICKEN);
            } else {
                currentPizza.removeMeat(Pizza.Meats.CHICKEN);
            }
            if(steak.isSelected()){
                currentPizza.addMeat(Pizza.Meats.STEAK);
            } else {
                currentPizza.removeMeat(Pizza.Meats.STEAK);
            }

            if(small.isSelected()){
                currentPizza.setSize(Pizza.Size.SMALL);
            }
            if(medium.isSelected()){
                currentPizza.setSize(Pizza.Size.MEDIUM);
            }
            if(large.isSelected()){
                currentPizza.setSize(Pizza.Size.LARGE);
            }

            currentPizzaLabel.setText(currentPizza.toString());
        }
    }

    /**
     * Class to add pizza to order
     */
    class AddToOrder implements EventHandler<ActionEvent>{

        /**
         * Adds pizza to order
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            pizzas.add(currentPizza);

            small.setSelected(true);
            medium.setSelected(false);
            large.setSelected(false);

            pepper.setSelected(false);
            onion.setSelected(false);
            mushroom.setSelected(false);
            olive.setSelected(false);
            bananaPepper.setSelected(false);

            ham.setSelected(false);
            pepperoni.setSelected(false);
            sausage.setSelected(false);
            salami.setSelected(false);
            chicken.setSelected(false);
            steak.setSelected(false);

            currentPizza = new Pizza();
            currentPizzaLabel.setText(currentPizza.toString());
        }
    }

    /**
     * Class to complete order
     */
    class CompleteOrder implements EventHandler<ActionEvent>{

        /**
         * Complete order
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            pizzas.add(currentPizza);
            StringBuilder output = new StringBuilder();
            Iterator<Pizza> iter = pizzas.iterator();
            double total = 0;
            while(iter.hasNext()){
                Pizza nextPizza = iter.next();
                output.append(nextPizza.toString());
                System.out.println(nextPizza);
                total += nextPizza.calculatePrice();
            }
            System.out.println("Total: $" + total*1.06);
            output.append("\nTotal: $" + total*1.06);

            finishedOrderText = new Label(output.toString());
            finishedOrderText.setPadding(new Insets(20));
            finishedOrder = new Scene(finishedOrderText);
            stage.setScene(finishedOrder);
        }
    }

}
