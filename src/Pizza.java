/**
 * Pizza.java
 * Object to store and manipulate pizza data
 *
 * @author Mason Hicks
 */

import java.util.ArrayList;
import java.util.Iterator;

public class Pizza {

    //Instance variables
    private ArrayList<Meats> meatToppings;
    private ArrayList<Veggies> veggieToppings;
    private Size size;

    /**
     * Constructor
     */
    public Pizza(){
        size = Size.SMALL;
        meatToppings = new ArrayList<>();
        veggieToppings = new ArrayList<>();
    }

    /**
     * Returns the formatted output for the objects
     * @return String output
     */
    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        Iterator<Meats> meatIterator = meatToppings.iterator();
        Iterator<Veggies> veggieIterator = veggieToppings.iterator();

        switch(size){
            case SMALL:
                output.append("1 Small Pizza\n");
                break;
            case MEDIUM:
                output.append("1 Medium Pizza\n");
                break;
            case LARGE:
                output.append("1 Large Pizza\n");
                break;
        }

        while(veggieIterator.hasNext()){
            switch(veggieIterator.next()){
                case OLIVE:
                    output.append("\tAdd Olives\n");
                    break;
                case ONION:
                    output.append("\tAdd Onions\n");
                    break;
                case PEPPER:
                    output.append("\tAdd Peppers\n");
                    break;
                case MUSHROOM:
                    output.append("\tAdd Mushrooms\n");
                    break;
                case BANANA_PEPPER:
                    output.append("\tAdd Bannana Peppers\n");
                    break;
            }
        }

        while(meatIterator.hasNext()){
            switch(meatIterator.next()){
                case HAM:
                    output.append("\tAdd Ham\n");
                    break;
                case STEAK:
                    output.append("\tAdd Steak\n");
                    break;
                case SALAMI:
                    output.append("\tAdd Salami\n");
                    break;
                case CHICKEN:
                    output.append("\tAdd Chicken\n");
                    break;
                case SAUSAGE:
                    output.append("\tAdd Sausage\n");
                    break;
                case PEPPERONI:
                    output.append("\tAdd Pepperoni\n");
                    break;
            }
        }
        output.append("Current Pizza Total: $" + calculatePrice() + "0");
        return output.toString() + "\n";
    }

    /**
     * Calculates the price
     * @return double price
     */
    public double calculatePrice(){
        double total = 0;

        switch(size){
            case SMALL:
                total += 10;
                break;
            case MEDIUM:
                total += 11;
                break;
            case LARGE:
                total += 12;
                break;
        }

        if(meatToppings.size() == 1) total += 2;
        else total += meatToppings.size()*1.5;

        total += veggieToppings.size() * 0.5;


        return total;
    }

    /**
     * Adds a veggie
     * @param v Pizza.Veggies veggie to add
     */
    public void addVeggie(Veggies v){
        removeVeggie(v);
        veggieToppings.add(v);
    }

    /**
     * Adds a meat
     * @param m Pizza.Meats meat to add
     */
    public void addMeat(Meats m){
        removeMeat(m);
        meatToppings.add(m);
    }

    /**
     * Removes a veggie
     * @param v Pizza.Veggies veggie to remove
     */
    public void removeVeggie(Veggies v){
        for(int i=0; i<veggieToppings.size(); i++){
            if(veggieToppings.get(i) == v){
                veggieToppings.remove(i);
                return;
            }
        }
    }

    /**
     * Removes a meat
     * @param m Pizza.Meats meat to remove
     */
    public void removeMeat(Meats m){
        for(int i=0; i<meatToppings.size(); i++){
            if(meatToppings.get(i) == m){
                meatToppings.remove(i);
                return;
            }
        }
    }

    /**
     * Sets the pizza size
     * @param s size
     */
    public void setSize(Size s){
        size = s;
    }

    /**
     * Meat types
     */
    enum Meats{
        HAM,
        PEPPERONI,
        SAUSAGE,
        SALAMI,
        CHICKEN,
        STEAK
    }

    /**
     * Veggie types
     */
    enum Veggies{
        PEPPER,
        ONION,
        MUSHROOM,
        OLIVE,
        BANANA_PEPPER
    }

    /**
     * Size types
     */
    enum Size{
        SMALL,
        MEDIUM,
        LARGE
    }
}
